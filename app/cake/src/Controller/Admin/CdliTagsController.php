<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * CdliTags Controller
 *
 * @property \App\Model\Table\CdliTagsTable $CdliTags
 *
 * @method \App\Model\Entity\CdliTagsTable[]
 */
class CdliTagsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $cdli_tags = $this->CdliTags->find('all', ['order' => 'cdli_tag'])->all();
        $cdli_tag = $this->CdliTags->newEntity();
        $this->set(compact('cdli_tags', 'cdli_tag'));
    }
    
    /**
     * Add method
     *
     * @return \Cake\Http\Response|void
     */
    public function add()
    {
        $data = $this->request->getData();
        debug($data);
        if ($data != []) {
            $cdli_tag = $this->CdliTags->newEntity();
            $cdli_tag = $this->CdliTags->patchEntity($cdli_tag, $data);
            debug($data);
            $duplicate_tag = $this->CdliTags->find()->where(['cdli_tag' => $cdli_tag->cdli_tag])->first();

            if ($duplicate_tag != []) {
                $this->Flash->error(__('Tag already exists.'));
            } else {
                if ($this->CdliTags->save($cdli_tag)) {
                    $this->Flash->success(__('New Tag has been saved.'));
                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('Unable to add your tag.'));
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Delete method
     *
     * @return \Cake\Http\Response|void
     */
    public function delete($id)
    {
        $cdli_tag = $this->CdliTags->find()->where(['id' => $id])->first();
        $result = $this->CdliTags->delete($cdli_tag);

        return $this->redirect(['action' => 'index']);
    }
}
