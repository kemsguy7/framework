<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Material $material
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($material) ?>
            <legend class="capital-heading"><?= __('Edit Material') ?></legend>
            <?php
                echo $this->Form->control('material');
                echo $this->Form->control('parent_id', ['options' => $parentMaterials, 'empty' => true]);
                // echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $material->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $material->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List Materials'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Parent Materials'), ['controller' => 'Materials', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Parent Material'), ['controller' => 'Materials', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
