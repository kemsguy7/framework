<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsExternalResource $artifactsExternalResource
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('View Artifacts External Resource') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsExternalResource->has('artifact') ? $this->Html->link($artifactsExternalResource->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsExternalResource->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('External Resource') ?></th>
                    <td><?= $artifactsExternalResource->has('external_resource') ? $this->Html->link($artifactsExternalResource->external_resource->id, ['controller' => 'ExternalResources', 'action' => 'view', $artifactsExternalResource->external_resource->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('External Resource Key') ?></th>
                    <td><?= h($artifactsExternalResource->external_resource_key) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsExternalResource->id) ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('Edit Artifacts External Resource'), ['action' => 'edit', $artifactsExternalResource->id], ['class' => 'btn-action']) ?>
        <?= $this->Form->postLink(__('Delete Artifacts External Resource'), ['action' => 'delete', $artifactsExternalResource->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsExternalResource->id), 'class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Artifacts External Resources'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifacts External Resource'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List External Resources'), ['controller' => 'ExternalResources', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New External Resource'), ['controller' => 'ExternalResources', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>



