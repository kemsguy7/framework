<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="row justify-content-md-center">

<div class="col-lg-7 login-box">
    <div class="capital-heading text-center">New Password</div>
    <?= $this->Flash->render() ?>

    <?= $this->Form->create() ?>
        <?= $this->Form->control('password',[
            'class' => 'col-12 input-background-child-md',
            'label' => 'New Password'
        ]) ?>
        <?= $this->Form->control('password_confirm', [
            'class' => 'col-12 input-background-child-md',
            'label'=>'Confirm New Password ',
            'type'=>'password'
        ]) ?>
        <div class="userloginbuttoncenter">
            <?= $this->Form->submit('submit', [
                'class' => 'btn cdli-btn-blue'
            ]) ?>
        </div> 

    <?= $this->Form->end() ?>
</div>

</div>