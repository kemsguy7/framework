<?php
namespace App\View\Helper;

use Cake\View\Helper;

class AccordionHelper extends Helper
{
    public function partOpen($id, $title, $tag="p")
    {
        return implode("\n", [
            '<section id="' . h($id) . '">',
            '<input type="checkbox" id="' . h($id) . '-toggle" class="accordion-textbox">',
            '<'.$tag.' class="accordion-title"><label class="w-100" for="' . h($id) . '-toggle">' . h($title) . '</label></'.$tag.'>',
            '<div class="detail-leaf">'
        ]);
    }

    public function partClose()
    {
        return '</div></section>';
    }
}
